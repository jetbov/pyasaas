import json
from unittest import TestCase
from pyasaas.model import Subscription

__author__ = 'zamboni'


class TestSubscription(TestCase):
    def setUp(self):
        self.json_string = '{"object":"subscription","id":"sub_V4ieW0O2W4OQ","customer":"cus_kLnRxbNfTFTD","value":799.0,"nextDueDate":"17/05/2017","cycle":"YEARLY","description":"Plano anual","billingType":"UNDEFINED","deleted":false,"status":"ACTIVE","payments":{"object":"list","hasMore":false,"limit":10,"offset":0,"data":[{"object":"payment","id":"pay_874180466613","customer":"cus_kLnRxbNfTFTD","subscription":"sub_V4ieW0O2W4OQ","value":799.0,"netValue":792.04,"originalValue":null,"nossoNumero":"88108339","description":null,"billingType":"UNDEFINED","status":"PENDING","dueDate":"17/05/2016","paymentDate":null,"invoiceUrl":"","boletoUrl":"","invoiceNumber":"00009064","externalReference":null,"deleted":false}]}}'
        self.json_object = json.loads(self.json_string)

    def test_from_json(self):
        subscription = Subscription()
        subscription.from_json(self.json_object)
        self.assertEqual(subscription.id, self.json_object[u'id'])
        self.assertEqual(subscription.value, self.json_object[u'value'])

    def test_to_json(self):
        json_expected = {
            u"customer": "cus_kLnRxbNfTFTD",
            u"value": 799.0,
            u"nextDueDate": "17/05/2017",
            u"cycle": "YEARLY",
            u"description": "Plano anual do JetBov",
            u"billingType": "UNDEFINED",
            u'status': '',
        }
        subscription = Subscription()
        subscription.customer = "cus_kLnRxbNfTFTD"
        subscription.value = 799.0
        subscription.next_due_date = "17/05/2017"
        subscription.cycle = "YEARLY"
        subscription.description = "Plano anual do JetBov"
        subscription.billing_type = "UNDEFINED"
        json_obj = subscription.to_json()
        self.assertDictEqual(json_expected, json_obj)
