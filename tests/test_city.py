from unittest import TestCase
from pyasaas.model import City

__author__ = 'zamboni'


class TestCity(TestCase):
    def setUp(self):
        self.city = City()
        self.json = {
            "object": "city",
            "id": 6198,
            "ibgeCode": "1711951",
            "name": "Lagoa do Tocantins",
            "districtCode": "05",
            "district": "Lagoa do Tocantins",
            "state": "TO"
        }
        self.city.from_json(self.json)

    def test_from_json(self):
        self.assertEqual(self.city.id, 6198)
        self.assertEqual(self.city.name, "Lagoa do Tocantins")
        self.assertEqual(self.city.state, "TO")
