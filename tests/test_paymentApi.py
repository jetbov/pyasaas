# coding=utf-8
import os
from unittest import TestCase
from datetime import datetime
from pyasaas import api
from pyasaas.api import CustomerApi, PaymentApi
from pyasaas.api import SubscriptionApi
from pyasaas.model import Customer, Subscription, Payment

__author__ = 'zamboni'


class TestPaymentApi(TestCase):
    def setUp(self):
        self.token = os.getenv('ASAAS_SECRET_KEY')
        self.env = api.ASAAS_HOMOLOG
        self.customer_api = CustomerApi(self.token, self.env)
        self.subscription_api = SubscriptionApi(self.token, self.env)
        self.payment_api = PaymentApi(self.token, self.env)
        self.customer = Customer()
        self.customer.name = "Mufunga Muadib"
        self.customer.email = "mufunga@muadib.com"
        self.customer.company = "Mufa"
        self.customer.phone = "(11) 2232-2222"
        self.customer.mobile_phone = "(11) 903903903"
        self.customer.address = "Rua Quarenta e Sete"
        self.customer.address_number = 33
        self.customer.complement = None
        self.customer.province = u"Jardim São Paulo(Zona Leste)"
        self.customer.foreign_customer = False
        self.customer.notification_disabled = False
        self.customer.city = 12565
        self.customer.state = "SP"
        self.customer.country = "Brasil"
        self.customer.postal_code = "89202110"
        self.customer.cpf_cnpj = "04398103961"
        self.customer.person_type = Customer.INDIVIDUAL_PERSON_TYPE
        self.customer = self.customer_api.save(self.customer)
        self.customer_id = self.customer.id
        self.customer_name = self.customer.name
        self.customer_email = self.customer.email
        self.subscription = Subscription()
        self.subscription.customer = self.customer.id
        self.subscription.value = 799.0
        self.subscription.next_due_date = "17/05/2017"
        self.subscription.cycle = "YEARLY"
        self.subscription.description = "Plano anual"
        self.subscription.billing_type = "UNDEFINED"
        self.subscription = self.subscription_api.save(self.subscription)
        self.payment = Payment()
        self.payment.customer = self.customer.id
        self.payment.subscription = self.subscription.id
        self.payment.billing_type = "BOLETO"
        self.payment.value = 99.0
        self.payment.due_date = datetime.now().strftime("%d/%m/%Y")
        self.payment.description = "Pagamento"
        self.payment = self.payment_api.save(self.payment)

    def test_get_by_id(self):
        payment = Payment()
        payment = self.payment_api.get_by_id(self.payment.id)
        self.assertEqual(payment.description, self.payment.description)
        self.assertEqual(payment.id, self.payment.id)
        self.assertEqual(payment.value, 99.0)

    def test_save(self):
        payment = Payment()
        self.payment.value = 199.0
        payment = self.payment_api.save(self.payment)
        self.assertEqual(payment.value, 199.0)

    def test_delete_by_id(self):
        payment = Payment()
        payment.customer = self.customer.id
        payment.subscription = self.subscription.id
        payment.billing_type = "BOLETO"
        payment.value = 99.0
        payment.due_date = datetime.now().strftime("%d/%m/%Y")
        payment.description = "Pagamento"
        payment = self.payment_api.save(payment)
        id = payment.id
        ret = self.payment_api.delete_by_id(id)
        self.assertEqual(ret, True)

    def tearDown(self):
        self.payment_api.delete_by_id(self.payment.id)
        self.subscription_api.delete_by_id(self.subscription.id)
        self.customer_api.delete_by_id(self.customer.id)
