import json
from unittest import TestCase
from pyasaas.model import Customer

__author__ = 'zamboni'


class TestCustomer(TestCase):
    def setUp(self):
        self.json = json.loads('{"object":"customer","id":"cus_kLnRxbNfTFTD","name":"Naruto Manata","email":"email@test.com","company":null,"phone":"(22) 33332222","mobilePhone":"(37) 9333813","address":"Rua","addressNumber":"51","complement":null,"province":"Ceu","postalCode":"33333-370","cpfCnpj":"00000000009","personType":"FISICA","deleted":false,"additionalEmails":null,"notificationDisabled":false,"city":122260,"state":"SP","country":"Brasil","foreignCustomer":false,"subscriptions":{"object":"list","hasMore":false,"limit":100,"offset":0,"data":[]},"payments":{"object":"list","hasMore":false,"limit":100,"offset":0,"data":[]},"notifications":{"object":"list","hasMore":false,"limit":100,"offset":0,"data":[{"object":"notification","id":"not_dpF1nfH1VUjU","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":true,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_RECEIVED","scheduleOffset":0,"deleted":false},{"object":"notification","id":"not_c6lMByE79ayp","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":true,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_OVERDUE","scheduleOffset":0,"deleted":false},{"object":"notification","id":"not_YQQubrJz22KA","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":false,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_DUEDATE_WARNING","scheduleOffset":10,"deleted":false},{"object":"notification","id":"not_X6MP8LcOELyO","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":false,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_DUEDATE_WARNING","scheduleOffset":0,"deleted":false},{"object":"notification","id":"not_4CVPGjT0X8vc","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":false,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_CREATED","scheduleOffset":0,"deleted":false},{"object":"notification","id":"not_9KrINAU6S8cl","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":false,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"PAYMENT_UPDATED","scheduleOffset":0,"deleted":false},{"object":"notification","id":"not_DrEqCh3UdATS","customer":"cus_kLnRxbNfTFTD","enabled":true,"emailEnabledForProvider":false,"smsEnabledForProvider":false,"emailEnabledForCustomer":true,"smsEnabledForCustomer":true,"event":"SEND_LINHA_DIGITAVEL","scheduleOffset":0,"deleted":false}]}}')

    def test_from_json(self):
        customer = Customer()
        customer.from_json(self.json)
        self.assertEqual(customer.name, "Naruto Manata")
        self.assertEqual(customer.email, "email@test.com")
        self.assertEqual(customer.state, "SP")
        self.assertEqual(customer.complement, None)

    def test_to_json(self):
        expected_json = {'province': '', 'notificationDisabled': False, 'postalCode': '', 'name': 'Joao da Silva', 'city': '', 'country': '', 'company': '', 'cpfCnpj': '', 'email': 'joao@teste.com', 'personType': '', 'phone': '', 'state': '', 'addressNumber': '', 'address': 'teste', 'complement': '', 'foreignCustomer': False, 'id': 'cus_384u9hf98234', 'mobilePhone': ''}
        customer = Customer()
        customer.id = "cus_384u9hf98234"
        customer.name = "Joao da Silva"
        customer.email = "joao@teste.com"
        customer.address = "teste"
        self.assertDictEqual(customer.to_json(), expected_json)
