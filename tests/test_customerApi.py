# coding=utf-8
import os
from unittest import TestCase
from pyasaas import api
from pyasaas.api import CustomerApi
from pyasaas.model import Customer

__author__ = 'zamboni'


class TestCustomerApi(TestCase):
    def setUp(self):
        self.token = os.getenv('ASAAS_SECRET_KEY')
        self.env = api.ASAAS_HOMOLOG
        self.customer_api = CustomerApi(self.token, self.env)

        self.customer2 = Customer()
        self.customer2.name = "Mufunga Muadib"
        self.customer2.email = "mufunga@muadib.com"
        self.customer2.company = "Mufa"
        self.customer2.phone = "(11) 2232-2222"
        self.customer2.mobile_phone = "(11) 903903903"
        self.customer2.address = "Rua Quarenta e Sete"
        self.customer2.address_number = 33
        self.customer2.complement = None
        self.customer2.province = u"Jardim São Paulo(Zona Leste)"
        self.customer2.foreign_customer = False
        self.customer2.notification_disabled = False
        self.customer2.city = 12565
        self.customer2.state = "SP"
        self.customer2.country = "Brasil"
        self.customer2.postal_code = "89202110"
        self.customer2.cpf_cnpj = "04398103961"
        self.customer2.person_type = Customer.INDIVIDUAL_PERSON_TYPE
        self.customer2 = self.customer_api.save(self.customer2)

        self.customer_id = self.customer2.id
        self.customer_name = self.customer2.name
        self.customer_email = self.customer2.email

    def test_get_by_id(self):
        customer = self.customer_api.get_by_id(self.customer_id)
        self.assertEqual(customer.email, self.customer_email)
        self.assertEqual(customer.name, self.customer_name)
        self.assertEqual(customer.id, self.customer_id)

    def test_get_by_email(self):
        customer = self.customer_api.get_by_email(self.customer_email)
        self.assertEqual(customer.email, self.customer_email)
        self.assertEqual(customer.name, self.customer_name)
        self.assertEqual(customer.id, self.customer_id)

    def test_save(self):
        self.customer2.name = "Umanga Bidoga"
        customer = self.customer_api.save(self.customer2)

        self.assertEqual(customer.email, self.customer2.email)
        self.assertEqual(customer.name, "Umanga Bidoga")
        self.assertEqual(customer.cpf_cnpj, self.customer2.cpf_cnpj)

        self.customer2.name = "Mufunga Muadib"
        customer = self.customer_api.save(self.customer2)
        self.assertEqual(customer.name, "Mufunga Muadib")

    def test_delete_by_id(self):
        customer2 = Customer()
        customer2.name = 'Person 1234'
        customer2.email = 'ppp@person.com'
        customer2.company = 'Person.com'
        customer2.phone = "(11) 2232-2222"
        customer2.mobile_phone = "(11) 903903903"
        customer2.address = "Rua Quarenta e Sete"
        customer2.address_number = 33
        customer2.complement = None
        customer2.province = u"Jardim São Paulo(Zona Leste)"
        customer2.foreign_customer = False
        customer2.notification_disabled = False
        customer2.city = 12565
        customer2.state = "SP"
        customer2.country = "Brasil"
        customer2.postal_code = "89202110"
        customer2.cpf_cnpj = "04398103961"
        customer2.person_type = Customer.INDIVIDUAL_PERSON_TYPE
        customer2.person_type = Customer.INDIVIDUAL_PERSON_TYPE
        customer = self.customer_api.save(customer2)
        self.customer_api.delete_by_id(customer.id)

    def tearDown(self):
        self.customer_api.delete_by_id(self.customer2.id)
