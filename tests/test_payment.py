import json
from unittest import TestCase
from pyasaas.model import Payment

__author__ = 'zamboni'


class TestPayment(TestCase):
    def setUp(self):
        self.json_string = json.loads('{"id":"pay_874180466613","customer":"cus_kLnRxbNfTFTD","subscription":"sub_V4ieW0O2W4OQ","value":799.0,"netValue":792.04,"originalValue":null,"nossoNumero":"88108339","description":null,"billingType":"UNDEFINED","status":"PENDING","dueDate":"17/05/2016","paymentDate":null,"invoiceUrl":"http://homolog.asaas.com/i/874180466613","boletoUrl":"http://homolog.asaas.com/b/pdf/874180466613","invoiceNumber":"00009064","externalReference":null,"deleted":false}')

    def test_from_json(self):
        payment = Payment()
        payment.from_json(self.json_string)
        self.assertEqual(payment.id, "pay_874180466613")
        self.assertEqual(payment.subscription, "sub_V4ieW0O2W4OQ")

    def test_to_json(self):
        expected_json = {
            u"customer": "",
            u"subscription": "",
            u"billingType": "",
            u"value": 99,
            u"netValue": 0,
            u"originalValue": 0,
            u"grossValue": 0,
            u"dueDate": "",
            u"status": "",
            u"nossoNumero": "",
            u"description": "",
            u"invoiceUrl": "",
            u"boletoUrl": "",
            u"installmentCount": 0,
            u"installmentValue": 0,
            u"externalReference": ""
        }
        payment = Payment()
        payment.customer = ""
        payment.subscription = ""
        payment.billing_type = ""
        payment.value = 99
        payment.net_value = 0
        payment.original_value = 0
        payment.gross_value = 0
        payment.due_date = ""
        payment.status = ""
        payment.nosso_numero = ""
        payment.description = ""
        payment.invoice_url = ""
        payment.boleto_url = ""
        payment.installment_count = 0
        payment.installment_value = 0
        payment.external_reference = ""
        json_object = payment.to_json()
        self.maxDiff = None
        self.assertDictEqual(json_object, expected_json)
