# coding=utf-8
import os
from unittest import TestCase
from pyasaas import api
from pyasaas.api import CustomerApi, SubscriptionApi
from pyasaas.model import Customer, Subscription

__author__ = 'zamboni'


class TestSubscriptionApi(TestCase):
    def setUp(self):
        self.token = os.getenv('ASAAS_SECRET_KEY')
        self.env = api.ASAAS_HOMOLOG
        self.customer_api = CustomerApi(self.token, self.env)
        self.subscription_api = SubscriptionApi(self.token, self.env)
        self.customer = Customer()
        self.customer.name = "Mufunga Muadib"
        self.customer.email = "mufunga@muadib.com"
        self.customer.company = "Mufa"
        self.customer.phone = "(11) 2232-2222"
        self.customer.mobile_phone = "(11) 903903903"
        self.customer.address = "Rua Quarenta e Sete"
        self.customer.address_number = 33
        self.customer.complement = None
        self.customer.province = u"Jardim São Paulo(Zona Leste)"
        self.customer.foreign_customer = False
        self.customer.notification_disabled = False
        self.customer.city = 12565
        self.customer.state = "SP"
        self.customer.country = "Brasil"
        self.customer.postal_code = "89202110"
        self.customer.cpf_cnpj = "04398103961"
        self.customer.person_type = Customer.INDIVIDUAL_PERSON_TYPE
        self.customer = self.customer_api.save(self.customer)
        self.customer_id = self.customer.id
        self.customer_name = self.customer.name
        self.customer_email = self.customer.email

        self.subscription = Subscription()
        self.subscription.customer = self.customer.id
        self.subscription.value = 799.0
        self.subscription.next_due_date = "17/05/2016"
        self.subscription.cycle = "YEARLY"
        self.subscription.description = "Plano anual Teste Subscription setUp"
        self.subscription.billing_type = "UNDEFINED"
        self.subscription = self.subscription_api.save(self.subscription)

    def test_get_by_id(self):
        subscription = self.subscription_api.get_by_id(self.subscription.id)
        self.assertEqual(subscription.description, "Plano anual Teste Subscription setUp")
        self.assertEqual(subscription.id, self.subscription.id)
        self.assertEqual(subscription.value, 799.0)

    def test_save(self):
        self.subscription.description = "Plano anual Teste Subscription setUp Novo"
        self.subscription.update_pending_payments = False
        subscription = self.subscription_api.save(self.subscription)
        self.assertEqual(subscription.description, "Plano anual Teste Subscription setUp Novo")

    def test_delete_by_id(self):
        subscription = Subscription()
        subscription.customer = self.customer.id
        subscription.value = 799.0
        subscription.next_due_date = "17/05/2016"
        subscription.cycle = "YEARLY"
        subscription.description = "Plano anual Test Subscription Delete"
        subscription.billing_type = "UNDEFINED"
        subscription = self.subscription_api.save(subscription)
        ret = self.subscription_api.delete_by_id(subscription.id)
        self.assertEqual(ret, True)

    def tearDown(self):
        self.subscription_api.delete_by_id(self.subscription.id)
        self.customer_api.delete_by_id(self.customer.id)
