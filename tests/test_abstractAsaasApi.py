from unittest import TestCase
from pyasaas import api
from pyasaas.api import AbstractAsaasApi

__author__ = 'zamboni'


class TestAbstractAsaasApi(TestCase):
    def setUp(self):
        super(TestAbstractAsaasApi, self).setUp()
        self.abstract_api = AbstractAsaasApi("123456", api.ASAAS_HOMOLOG)

    def test___init__(self):
        self.assertEqual(self.abstract_api.environment, api.ASAAS_HOMOLOG)
        self.assertEqual(self.abstract_api.secret_token, "123456")
