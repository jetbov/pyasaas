import os
from unittest import TestCase
from pyasaas import api
from pyasaas.api import CityApi

__author__ = 'zamboni'


class TestCityApi(TestCase):
    def setUp(self):
        self.token = os.getenv('ASAAS_SECRET_KEY')
        self.env = api.ASAAS_HOMOLOG
        self.city_api = CityApi(self.token, self.env)
        self.city_id = 6198

    def test_get_by_id(self):
        city = self.city_api.get_by_id(self.city_id)
        self.assertEqual(city.id, 6198)
        self.assertEqual(city.name, "Lagoa do Tocantins")
        self.assertEqual(city.state, "TO")
