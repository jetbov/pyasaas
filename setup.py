try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

requirements = open('requirements.txt').read().splitlines()
long_description = open('README.rst').read()

config = {
    'description': 'PyASaaS',
    'long_description': long_description,
    'author': 'Giovani Zamboni',
    'url': 'https://github.com/gzamboni/pyasaas',
    'download_url': 'https://github.com/gzamboni/pyasaas',
    'author_email': 'zamboni@jetbov.com',
    'version': '0.3',
    'install_requires': requirements,
    'packages': find_packages(exclude=['tests', 'tests.*']),
    'scripts': [],
    'name': 'pyasaas',
    'classifiers': [
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ]
}


setup(**config)
