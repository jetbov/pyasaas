# coding=utf-8
import json
import requests
from pyasaas.model import City, Customer, Subscription, Payment

__author__ = "zamboni"

(
    ASAAS_PRODUCTION,
    ASAAS_HOMOLOG
) = range(2)


class AbstractAsaasApi(object):
    HOMOLOG_ENDPOINT = "http://homolog.asaas.com/api/v2"
    PRODUCTION_ENDPOINT = "https://www.asaas.com/api/v2"

    def __init__(self, secret_token, environment):
        self._secret_token = secret_token
        self._environment = environment

    @property
    def secret_token(self):
        return self._secret_token

    @secret_token.setter
    def secret_token(self, value):
        self._secret_token = value

    @property
    def environment(self):
        return self._environment

    @environment.setter
    def environment(self, value):
        self._environment = value

    @property
    def endpoint(self):
        return self.HOMOLOG_ENDPOINT if self.environment == ASAAS_HOMOLOG else self.PRODUCTION_ENDPOINT


class CityApi(AbstractAsaasApi):
    def get_by_id(self, city_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/cities/%s" % (self.endpoint, city_id), headers=headers)
            if request.status_code == requests.codes.ok:
                city = City()
                city.from_json(request.json())
                return city
            else:
                raise StandardError("Error on parse city return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def get_by_name(self, city_name):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/cities/?name=%s" % (self.endpoint, city_name), headers=headers)
            if request.status_code == requests.codes.ok:
                return_data = request.json()
                if return_data.get("object") == "list":
                    list_data = return_data.get("data")
                    if len(list_data) > 0:
                        city = City()
                        city.from_json(list_data[0]["city"])
                        return city
                    else:
                        raise StandardError("No city return for %s" % request.reason)
            else:
                raise StandardError("Error on parse city return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")


class CustomerApi(AbstractAsaasApi):
    def get_by_id(self, customer_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/customers/%s" % (self.endpoint, customer_id), headers=headers)
            if request.status_code == requests.codes.ok:
                customer = Customer()
                customer.from_json(request.json())
                return customer
            else:
                raise StandardError("Error on parse customer return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def get_by_email(self, customer_email):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            filter_data = "{'name' : %s}" % customer_email
            request = requests.get("%s/customers" % self.endpoint, headers=headers, data=filter_data)
            if request.status_code == requests.codes.ok:
                customer = Customer()
                json_response = request.json()
                assert isinstance(json_response, dict)
                if 'object' in json_response.keys():
                    if json_response['data'].__len__ > 0:
                        json_customer = json_response['data'][0]['customer']
                        customer.from_json(json_customer)
                        return customer
                else:
                        customer.from_json(json_response)
                        return customer
            else:
                raise StandardError("Error on parse customer return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def save(self, customer):
        if self.secret_token is not None:
            assert isinstance(customer, Customer)
            headers = {'access_token': self.secret_token}
            customer_json = json.dumps(customer.to_json())
            if customer.id is not None:
                endpoint_string = "%s/customers/%s" % (self.endpoint, customer.id)
            else:
                endpoint_string = "%s/customers" % self.endpoint
            request = requests.post(endpoint_string, headers=headers, data=customer_json)
            if request.status_code == requests.codes.ok:
                customer = Customer()
                customer.from_json(request.json())
                return customer
            else:
                raise StandardError("Error on save customer return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def delete_by_id(self, customer_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.delete("%s/customers/%s" % (self.endpoint, customer_id), headers=headers)
            if request.status_code == requests.codes.ok:
                return True
            else:
                raise StandardError("Error on parse customer return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")


class SubscriptionApi(AbstractAsaasApi):
    def get_by_id(self, subscription_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/subscriptions/%s" % (self.endpoint, subscription_id), headers=headers)
            if request.status_code == requests.codes.ok:
                subscription = Subscription()
                subscription.from_json(request.json())
                return subscription
            else:
                raise StandardError("Error on parse subscription return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def save(self, subscription):
        if self.secret_token is not None:
            assert isinstance(subscription, Subscription)
            headers = {'access_token': self.secret_token}
            subscription_json = json.dumps(subscription.to_json())
            if subscription.id is not None:
                endpoint_string = "%s/subscriptions/%s" % (self.endpoint, subscription.id)
            else:
                endpoint_string = "%s/subscriptions" % self.endpoint
            request = requests.post(endpoint_string, headers=headers, data=subscription_json)
            if request.status_code == requests.codes.ok:
                subscription = Subscription()
                subscription.from_json(request.json())
                return subscription
            else:
                raise StandardError("Error on save subscription return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def delete_by_id(self, subscription_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}

            payments_api = PaymentApi(self.secret_token, self.environment)
            payments = self.get_payments_by_id(subscription_id)

            while payments.__len__() > 0:
                for json_payment in payments:
                    payments_api.delete_by_id(json_payment[u"id"])
                payments = self.get_payments_by_id(subscription_id)

            request = requests.delete("%s/subscriptions/%s" % (self.endpoint, subscription_id), headers=headers)
            if request.status_code == requests.codes.ok:
                return True
            else:
                raise StandardError("Error on parse subscription return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def get_payments_by_id(self, subscription_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/subscriptions/%s/payments" % (self.endpoint, subscription_id), headers=headers)
            if request.status_code == requests.codes.ok:
                payments = request.json()
                return payments[u"data"]
            else:
                raise StandardError("Error on parse subscription return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")


class PaymentApi(AbstractAsaasApi):
    def get_by_id(self, payment_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.get("%s/payments/%s" % (self.endpoint, payment_id), headers=headers)
            if request.status_code == requests.codes.ok:
                payment = Payment()
                payment.from_json(request.json())
                return payment
            else:
                raise StandardError("Error on parse payment return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def save(self, payment):
        if self.secret_token is not None:
            assert isinstance(payment, Payment)
            headers = {'access_token': self.secret_token}
            payment_json = json.dumps(payment.to_json())
            if payment.id is not None:
                endpoint_string = "%s/payments/%s" % (self.endpoint, payment.id)
            else:
                endpoint_string = "%s/payments" % self.endpoint
            request = requests.post(endpoint_string, headers=headers, data=payment_json)
            if request.status_code == requests.codes.ok:
                payment = Payment()
                payment.from_json(request.json())
                return payment
            else:
                raise StandardError("Error on save payment return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")

    def delete_by_id(self, payment_id):
        if self.secret_token is not None:
            headers = {'access_token': self.secret_token}
            request = requests.delete("%s/payments/%s" % (self.endpoint, payment_id), headers=headers)
            if request.status_code == requests.codes.ok:
                return True
            else:
                raise StandardError("Error on parse payment return. %s" % request.reason)
        else:
            raise StandardError("No ASaaS secret token defined")
