__author__ = 'zamboni'


class City(object):
    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def ibge_code(self):
        return self._ibge_code

    @ibge_code.setter
    def ibge_code(self, value):
        self._ibge_code = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def district_code(self):
        return self._district_code

    @district_code.setter
    def district_code(self, value):
        self._district_code = value

    @property
    def district(self):
        return self._district

    @district.setter
    def district(self, value):
        self._district = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value

    def __init__(self):
        self._id = None
        self._ibge_code = ""
        self._name = ""
        self._district_code = ""
        self._district = ""
        self._state = ""

    def from_json(self, json):
        self._id = json[u'id']
        self._ibge_code = json[u'ibgeCode']
        self._name = json[u'name']
        self._district = json[u'district']
        self._district_code = json[u'districtCode']
        self._state = json[u'state']


class Customer(object):
    INDIVIDUAL_PERSON_TYPE = 'FISICA'
    LEGAL_PERSON_PERSON_TYPE = 'JURIDICA'

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, value):
        self._email = value

    @property
    def company(self):
        return self._company

    @company.setter
    def company(self, value):
        self._company = value

    @property
    def phone(self):
        return self._phone

    @phone.setter
    def phone(self, value):
        self._phone = value

    @property
    def mobile_phone(self):
        return self._mobile_phone

    @mobile_phone.setter
    def mobile_phone(self, value):
        self._mobile_phone = value

    @property
    def address(self):
        return self._address

    @address.setter
    def address(self, value):
        self._address = value

    @property
    def address_number(self):
        return self._address_number

    @address_number.setter
    def address_number(self, value):
        self._address_number = value

    @property
    def complement(self):
        return self._complement

    @complement.setter
    def complement(self, value):
        self._complement = value

    @property
    def province(self):
        return self._province

    @province.setter
    def province(self, value):
        self._province = value

    @property
    def foreign_customer(self):
        return self._foreign_customer

    @foreign_customer.setter
    def foreign_customer(self, value):
        self._foreign_customer = value

    @property
    def notification_disabled(self):
        return self._notification_disabled

    @notification_disabled.setter
    def notification_disabled(self, value):
        self._notification_disabled = value

    @property
    def city(self):
        return self._city

    @city.setter
    def city(self, value):
        self._city = value

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value

    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, value):
        self._country = value

    @property
    def postal_code(self):
        return self._postal_code

    @postal_code.setter
    def postal_code(self, value):
        self._postal_code = value

    @property
    def cpf_cnpj(self):
        return self._cpf_cnpj

    @cpf_cnpj.setter
    def cpf_cnpj(self, value):
        self._cpf_cnpj = value

    @property
    def person_type(self):
        return self._person_type

    @person_type.setter
    def person_type(self, value):
        self._person_type = value

    @property
    def subscriptions(self):
        return self._subscriptions

    @subscriptions.setter
    def subscriptions(self, value):
        self._subscriptions = value

    @property
    def payments(self):
        return self._payments

    @payments.setter
    def payments(self, value):
        self._payments = value

    @property
    def notifications(self):
        return self._notifications

    @notifications.setter
    def notifications(self, value):
        self._notifications = value

    def __init__(self):
        self._id = 0
        self._name = ""
        self._email = ""
        self._company = ""
        self._phone = ""
        self._mobile_phone = ""
        self._address = ""
        self._address_number = ""
        self._complement = ""
        self._province = ""
        self._foreign_customer = False
        self._notification_disabled = False
        self._city = ""
        self._state = ""
        self._country = ""
        self._postal_code = ""
        self._cpf_cnpj = ""
        self._person_type = ""
        self._subscriptions = []
        self._payments = []
        self._notifications = []

    def from_json(self, json):
        self.id = json[u'id']
        self.name = json[u'name']
        self.email = json[u'email']
        self.company = json[u'company']
        self.phone = json[u'phone']
        self.mobile_phone = json[u'mobilePhone']
        self.address = json[u'address']
        self.address_number = json[u'addressNumber']
        self.complement = json[u'complement']
        self.province = json[u'province']
        self.foreign_customer = json[u'foreignCustomer']
        self.notification_disabled = json[u'notificationDisabled']
        self.city = json[u'city']
        self.state = json[u'state']
        self.country = json[u'country']
        self.postal_code = json[u'postalCode']
        self.cpf_cnpj = json[u'cpfCnpj']
        self.person_type = json[u'personType']

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'company': self.company,
            'phone': self.phone,
            'mobilePhone': self.mobile_phone,
            'address': self.address,
            'addressNumber': self.address_number,
            'complement': self.complement,
            'province': self.province,
            'foreignCustomer': self.foreign_customer,
            'notificationDisabled': self.notification_disabled,
            'city': self.city,
            'state': self.state,
            'country': self.country,
            'postalCode': self.postal_code,
            'cpfCnpj': self.cpf_cnpj,
            'personType': self.person_type
        }


class Subscription(object):
    MONTHLY = "MONTHLY"
    QUARTERLY = "QUARTERLY"
    SEMIANNUALLY = "SEMIANNUALLY"
    YEARLY = "YEARLY"
    WEEKLY = "WEEKLY"
    BIWEEKLY = "BIWEEKLY"

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @property
    def customer(self):
        return self._customer

    @customer.setter
    def customer(self, value):
        self._customer = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    @property
    def gross_value(self):
        return self._gross_value

    @gross_value.setter
    def gross_value(self, value):
        self._gross_value = value

    @property
    def next_due_date(self):
        return self._next_due_date

    @next_due_date.setter
    def next_due_date(self, value):
        self._next_due_date = value

    @property
    def cycle(self):
        return self._cycle

    @cycle.setter
    def cycle(self, value):
        self._cycle = value

    @property
    def billing_type(self):
        return self._billing_type

    @billing_type.setter
    def billing_type(self, value):
        self._billing_type = value

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def update_pending_payments(self):
        return self._update_pending_payments

    @update_pending_payments.setter
    def update_pending_payments(self, value):
        self._update_pending_payments = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value):
        self._status = value

    @property
    def payments(self):
        return self._payments

    @payments.setter
    def payments(self, value):
        self._payments = value

    @property
    def max_payments(self):
        return self._max_payments

    @max_payments.setter
    def max_payments(self, value):
        self._max_payments = value

    def __init__(self):
        self._id = ""
        self._billing_type = ""
        self._customer = ""
        self._cycle = ""
        self._description = ""
        self._next_due_date = ""
        self._payments = []
        self._status = ""
        self._value = 0

    def from_json(self, json):
        self.id = json[u"id"]
        self.billing_type = json[u"billingType"]
        self.customer = json[u"customer"]
        self.cycle = json[u"cycle"]
        self.description = json[u"description"]
        self.next_due_date = json[u"nextDueDate"]
        self.status = json[u"status"]
        self.value = json[u"value"]
        for payment_json in json[u"payments"][u"data"]:
            payment = Payment()
            payment.from_json(payment_json)
            self.payments.append(payment)

    def to_json(self):
        return {
            u"billingType": self.billing_type,
            u"customer": self.customer,
            u"cycle": self.cycle,
            u"description": self.description,
            u"nextDueDate": self.next_due_date,
            u"status": self.status,
            u"value": self.value
        }


class Payment(object):
    @property
    def id(self):
        return self._id
    
    @id.setter
    def id(self, value):
        self._id = value
    
    @property
    def customer(self):
        return self._customer
    
    @customer.setter
    def customer(self, value):
        self._customer = value
    
    @property
    def subscription(self):
        return self._subscription
    
    @subscription.setter
    def subscription(self, value):
        self._subscription = value
    
    @property
    def installment(self):
        return self._installment
    
    @installment.setter
    def installment(self, value):
        self._installment = value
    
    @property
    def billing_type(self):
        return self._billing_type
    
    @billing_type.setter
    def billing_type(self, value):
        self._billing_type = value
    
    @property
    def value(self):
        return self._value
    
    @value.setter
    def value(self, value):
        self._value = value
    
    @property
    def net_value(self):
        return self._net_value
    
    @net_value.setter
    def net_value(self, value):
        self._net_value = value
    
    @property
    def original_value(self):
        return self._original_value
    
    @original_value.setter
    def original_value(self, value):
        self._original_value = value
    
    @property
    def interest_value(self):
        return self._interest_value
    
    @interest_value.setter
    def interest_value(self, value):
        self._interest_value = value
    
    @property
    def gross_value(self):
        return self._gross_value
    
    @gross_value.setter
    def gross_value(self, value):
        self._gross_value = value
    
    @property
    def due_date(self):
        return self._due_date

    @due_date.setter
    def due_date(self, value):
        self._due_date = value

    @property
    def status(self):
        return self._status
    
    @status.setter
    def status(self, value):
        self._status = value
    
    @property
    def nosso_numero(self):
        return self._nosso_numero
    
    @nosso_numero.setter
    def nosso_numero(self, value):
        self._nosso_numero = value
    
    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, value):
        self._description = value

    @property
    def invoice_url(self):
        return self._invoice_url

    @invoice_url.setter
    def invoice_url(self, value):
        self._invoice_url = value

    @property
    def boleto_url(self):
        return self._boleto_url
    
    @boleto_url.setter
    def boleto_url(self, value):
        self._boleto_url = value
    
    @property
    def invoice_number(self):
        return self._invoice_number
    
    @invoice_number.setter
    def invoice_number(self, value):
        self._invoice_number = value
    
    @property
    def installment_count(self):
        return self._installment_count
    
    @installment_count.setter
    def installment_count(self, value):
        self._installment_count = value
    
    @property
    def installment_value(self):
        return self._installment_value
    
    @installment_value.setter
    def installment_value(self, value):
        self._installment_value = value
    
    @property
    def external_reference(self):
        return self._external_reference
    
    @external_reference.setter
    def external_reference(self, value):
        self._external_reference = value
    
    def __init__(self):
        self._id = ""
        self._customer = ""
        self._subscription = ""
        self._billing_type = ""
        self._value = 0
        self._net_value = 0
        self._original_value = 0
        self._gross_value = 0
        self._due_date = ""
        self._status = ""
        self._nosso_numero = 0
        self._description = ""
        self._invoice_url = ""
        self._boleto_url = ""
        self._invoice_number = ""
        self._installment_count = 0
        self._installment_value = 0
        self._external_reference = ""

    def from_json(self, json):
        self.id = json.get(u'id', None)
        self.customer = json.get(u'customer', None)
        self.subscription = json.get(u'subscription', None)
        self.installment = json.get(u'installment', None)
        self.billing_type = json.get(u'billingType', None)
        self.value = json.get(u'value', None)
        self.net_value = json.get(u'netValue', None)
        self.original_value = json.get(u'originalValue', None)
        self.interest_value = json.get(u'interestValue', None)
        self.gross_value = json.get(u'grossValue', None)
        self.due_date = json.get(u'dueDate', None)
        self.status = json.get(u'status', None)
        self.nosso_numero = json.get(u'nossoNumero', None)
        self.description = json.get(u'description', None)
        self.invoice_url = json.get(u'invoiceUrl', None)
        self.boleto_url = json.get(u'boletoUrl', None)
        self.invoice_number = json.get(u'invoiceNumber', None)
        self.installment_count = json.get(u'installmentCount', None)
        self.installment_value = json.get(u'installmentValue', None)
        self.external_reference = json.get(u'externalReference', None)

    def to_json(self):
        return {
            u"customer": self.customer,
            u"subscription": self.subscription,
            u"billingType": self.billing_type,
            u"value": self.value,
            u"netValue": self.net_value,
            u"originalValue": self.original_value,
            u"grossValue": self.gross_value,
            u"dueDate": self.due_date,
            u"status": self.status,
            u"nossoNumero": self.nosso_numero,
            u"description": self.description,
            u"invoiceUrl": self.invoice_url,
            u"boletoUrl": self.boleto_url,
            u"installmentCount": self.installment_count,
            u"installmentValue": self.installment_value,
            u"externalReference": self.external_reference
        }
